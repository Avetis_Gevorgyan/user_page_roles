<?php

namespace backend\controllers;

use common\models\PageRole;
use Yii;
use common\models\Page;
use backend\models\PageSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends SiteController
{


    /**
     * @inheritdoc
     */


    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            foreach ($model->roles as $role){
                $page_roul = new PageRole();
                $page_roul->page_id = $model->id;
                $page_roul->role_id = $role;
                $page_roul->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $roles = ArrayHelper::getColumn($model->getRoles()->asArray()->all(), 'name');
        if ($model->load(Yii::$app->request->post())&&  $model->save()) {
                if(!empty($model->roles)){
                    $this->Roles($model->roles, $model->id);
                } else {
                    $this->Roles($roles, $model->id);
                }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->roles = $model->findOne($id)->getRoles()->all();
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    protected function Roles($roles, $id){
        PageRole::deleteAll(['page_id'=>$id]);
        foreach ($roles as $role){
            $user_role = new PageRole();
            $user_role->page_id = $id;
            $user_role->role_id = $role;
            $user_role->save();
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        PageRole::deleteAll(['page_id'=>$id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
