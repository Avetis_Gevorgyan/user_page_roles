<?php

namespace backend\controllers;

use common\models\UserRoles;
use Yii;
use common\models\User;
use backend\models\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use zxbodya\yii2\galleryManager\GalleryManagerAction;


/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'galleryApi' => [
                'class' => GalleryManagerAction::className(),
                // mappings between type names and model classes (should be the same as in behaviour)
                'types' => [
                    'user' => User::className()
                ]
            ],
        ];
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
            if($model->save()){
                foreach ($model->roles as $role){
                    $user_role = new UserRoles();
                    $user_role->user_id = $model->id;
                    $user_role->role_id = $role;
                    $user_role->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create', [ 'model' => $model]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'adminUpdate';
        $password = $model->password_hash;
        $roles = ArrayHelper::getColumn($model->getRoles()->asArray()->all(), 'id');
        if ($model->load(Yii::$app->request->post())) {
            if(!empty($model->password_hash)) {
                $model->save();
                if(!empty($model->roles)){
                    $this->Roles($model->roles, $model->id);
                } else {
                    $this->Roles($roles, $model->id);
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else{
                    $model->password_hash = $password;
                    $model->save();
                    if(!empty($model->roles)){

                        $this->Roles($model->roles, $model->id);
                    } else {
                        $this->Roles($roles, $model->id);
                    }
                }

                return $this->redirect(['view', 'id' => $model->id]);

        } else {
            $model->password_hash = '';
            $model->roles = $model->findOne($id)->getRoles()->all();
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

protected function Roles($roles, $id){
    UserRoles::deleteAll(['user_id'=>$id]);
    foreach ($roles as $role){
        $user_role = new UserRoles();
        $user_role->user_id = $id;
        $user_role->role_id = $role;
        $user_role->save();
    }
}
    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        UserRoles::deleteAll(['user_id'=>$id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
           return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
