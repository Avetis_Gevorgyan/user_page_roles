<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "123456789_files".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $type
 * @property integer $size
 *
 * @property Message $user
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '123456789_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'type', 'size'], 'required'],
            [['user_id', 'size'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'type' => 'Type',
            'size' => 'Size',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Message::className(), ['id' => 'user_id']);
    }
}
