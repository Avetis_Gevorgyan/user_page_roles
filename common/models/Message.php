<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
/**
 * This is the model class for table "{{%message}}".
 *
 * @property integer $id
 * @property integer $sending_id
 * @property integer $recipient_id
 * @property string $message
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $type
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
public  $filesUpload;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sending_id', 'recipient_id', 'status', 'type'], 'required'],
            [[ 'message'], 'required', 'on' => 'message'],
            [['sending_id', 'recipient_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['message', 'type'], 'string'],
            [['filesUpload'], 'file'],
            [['message', 'sending_id', 'recipient_id', 'status'], 'filter','filter' => function($value){ return strip_tags($value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sending_id' => 'Sending ID',
            'recipient_id' => 'Recipient ID',
            'message' => 'Message',
            'status' => 'Status',
        ];
    }

    public function getUsersSending(){
        return $this->hasMany(User::className(), ['id'=> 'sending_id']);
    }

    public function getUsersRecipient(){
        return $this->hasMany(User::className(), [ 'id' => 'recipient_id']);
    }

    public function uploadFile($files)
    {
        if($this->validate()){
            $path = Yii::getAlias('@frontend') . '/web/files/'.$this->sending_id.'/'.$this->recipient_id;
            FileHelper::createDirectory($path, $mode = 0775, $recursive = true);

            foreach($files as $file){
           return json_encode($file);
             //   $type = explode('/', $file['type']);
               /* $model = new Files();
                $model->user_id = $this->id;
                $model->name = $this->created_at .'.'.$type[1];
                $model->type = $file['type'];
                $model->size = $file['size'];
                $model->saveAs($path.'/'.$model->name);
                $model->save();*/
            }
        //return json_encode();

          //  $this->filesUpload->saveAs($path.'/'. $this->created_at .'.'.$this->filesUpload->extension);
          //  return json_encode($files);
        }
   }
}
