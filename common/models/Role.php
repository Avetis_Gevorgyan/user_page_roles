<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%roles}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property UserRoles[] $userRoles
 */
class Role extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%roles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles(){
            return $this->hasMany(UserRoles::className(), ['role_id' => 'id']);
        }

    public function getPagesRoles()
    {
        return $this->hasMany(PageRole::className(), ['role_id' => 'id']);
    }

    public function getPages()
    {
        return $this->hasMany(Page::className(), ['id' => 'page_id'])->via('pagesRoles');
    }
}
