<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_roles`.
 */
class m170503_070239_create_user_roles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user_roles}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('user_roles_user', '{{%user_roles}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('user_roles_role', '{{%user_roles}}', 'role_id', '{{%roles}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('user_roles_user', '{{%user_roles}}');
        $this->dropForeignKey('user_roles_role', '{{%user_roles}}');
        $this->dropTable('{{%user_roles}}');
    }
}
