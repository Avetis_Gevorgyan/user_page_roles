<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages_roles`.
 */
class m170503_090646_create_pages_roles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%pages_roles}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'role_id' =>$this->integer()->notNull(),
        ]);
        $this->addForeignKey('pages_roles_page', '{{%pages_roles}}', 'page_id', '{{%pages}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('pages_roles_role', '{{%pages_roles}}', 'role_id', '{{%roles}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('pages_roles_page', '{{%pages_roles}}');
        $this->dropForeignKey('pages_roles_role', '{{%pages_roles}}');
        $this->dropTable('{{%pages_roles}}');
    }
}
