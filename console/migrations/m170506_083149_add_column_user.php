<?php

use yii\db\Migration;

class m170506_083149_add_column_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'image', $this->string(64));
    }

    public function down()
    {
        echo "m170506_083149_add_column_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
