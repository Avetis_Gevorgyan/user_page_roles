<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m170512_130732_create_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'sending_id' => $this->integer()->notNull(),
            'recipient_id' => $this->integer()->notNull(),
            'message' => $this->text(),
            'status' => $this->integer()->notNull()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%message}}');
    }
}
