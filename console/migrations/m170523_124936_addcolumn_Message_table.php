<?php

use yii\db\Migration;

class m170523_124936_addcolumn_Message_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%message}}', 'type', $this->string()->after('message'));
    }

    public function down()
    {
        echo "m170523_124936_addcolumn_Message_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
