<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files`.
 */
class m170524_091244_create_files_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%files}}', [
            'id' => $this->primaryKey(),
            'user_id' =>$this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'size' => $this->integer()->notNull()
        ]);
        $this->addForeignKey("Files", "{{%files}}", "user_id", "{{%message}}", "id", "CASCADE", "CASCADE");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey("Files", "{{%files}}");
        $this->dropTable('{{%files}}');
    }
}
