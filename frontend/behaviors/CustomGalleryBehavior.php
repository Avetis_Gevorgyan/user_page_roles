<?php
namespace frontend\behaviors;

use zxbodya\yii2\galleryManager\GalleryBehavior;

class CustomGalleryBehavior extends GalleryBehavior
{
    public function afterUpdate()
    {
        $galleryId = $this->getGalleryId();
        if ($this->_galleryId && $this->_galleryId != $galleryId) {
            $dirPath1 = $this->directory . '/' . $this->_galleryId;
            $dirPath2 = $this->directory . '/' . $galleryId;
            rename($dirPath1, $dirPath2);
        }
    }
}