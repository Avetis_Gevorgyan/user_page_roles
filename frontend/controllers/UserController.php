<?php

namespace frontend\controllers;

use common\models\Files;
use common\models\Message;
use Yii;
use common\models\User;
use frontend\models\UserSearch;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends SiteController
{
    /**
     * @inheritdoc
     */


    /**
     * Lists all User models.
     * @return mixed
     */
    private $count = 0;

    public function actionIndex()
    {
        $messages = '';
        $recipientId = 0;
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $id = yii::$app->user->id;
        $count = Message::find()
            ->where(['recipient_id' => $id, 'status' => 0])
            ->count();
        if (Yii::$app->request->get('user')) {
            $recipient_id = Yii::$app->request->get('user');
            $userIds = array_keys($searchModel->find()->indexBy('id')->asArray()->all());
            $sending_id = yii::$app->user->id;
            if (in_array($recipient_id, $userIds)) {
                $messages1 = $searchModel->findOne($sending_id)->getSendings($recipient_id)->indexBy('id')->all();
                $messages2 = $searchModel->findOne($sending_id)->getRecipients($recipient_id)->indexBy('id')->all();
                $messages = $messages1 + $messages2;
                ksort($messages);
                $recipientId = array_keys($searchModel->findOne($sending_id)->getRecipients($recipient_id)->select('id')->indexBy('id')->asArray()->all());
            }
        }
        return $this->render('index', compact(
            'searchModel', 'dataProvider', 'count',
            'messages', 'recipientId'
        ));
    }

    public function actionFileUpload()
    {
      /*  $model = new Message();
        if ($model->load(Yii::$app->request->post())) {
                $files = new Files();
                $files->user_id = $model->id;
                $files->name = "{$model->created_at}.{$model->filesUpload->extension}";
                $files->type = $model->filesUpload->type;
                $files->size = $model->filesUpload->size;
                $files->save();
                return json_encode(['success' => true]);
            }*/
        }


    public function actionSendMessages()
    {
        $model = new Message();
        $model->scenario = 'message';
        if ($model->load(Yii::$app->request->post())) {
            if (User::findOne(['id' => $model->sending_id]) && User::findOne(['id' => $model->recipient_id])) {
                $model->save();
                if ($model->type && $model->type == 'files') {
                    if(isset($_FILES)) {
                        print_r($_FILES);
                    }
                //   $model->uploadFile(Yii::$app->request->post('files'));

                   // return $model->insertFile(Yii::$app->request->post('msgFiles'));
                }
                return json_encode(["message" => 'sent', "id" => $model->id]);
            }
            return $this->redirect('index');
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'userCreate';
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            $model->generateAuthKey();

            if ($model->save()) {
                if (Yii::$app->getUser()->login($model)) {
                    return $this->goHome();
                }
            } else {
                return $this->render('create', ['model' => $model]);
            }
        } else {
            return $this->render('create', ['model' => $model]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate()
    {
        $id = \Yii::$app->user->id;
        $model = $this->findModel($id);
        $model->scenario = 'userUpdate';
        $password = $model->password_hash;

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->password)) {
                $model->setPassword($password);
            } else {
                $model->setPassword($model->password);
            }
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if (!empty($model->imageFile)) {
                $model->upload();
            }
            $model->save();
            return $this->redirect('index');
        } else {
            $model->password = '';
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        if (Yii::$app->request->post('id')) {

            $id = Yii::$app->request->post('id');
            Message::findOne($id)->delete();
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
