<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container">

    <ul class="nav nav-pills">
        <?php foreach($pages as $page): ?>
            <li><?= \yii\helpers\Html::a( $page->title,['view', 'id'=>$page->id])  ?></li>
        <?php endforeach; ?>
    </ul>
</div>
</body>
</html>
