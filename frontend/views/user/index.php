<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\file\FileInput;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = yii::$app->user->identity->username;
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="container">
        <div class="col-lg-4">
            <div>

                <?= Html::a(Html::img($searchModel->setImage(yii::$app->user->identity->id), [
                    'alt' => 'My logo',
                    'class' => 'img-circle',
                    'width' => "100",
                    'height' => "60"
                ]), ['update']) ?>
                <h6><?= yii::$app->user->identity->username ?> <div class="count" ><?= $count ?></div></h6>
            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute'=> '',
                        'filter' => \kartik\select2\Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'username',
                            'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->all(), 'username', 'username' ),
                            'options' => ['placeholder' => 'Users...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]),
                        'format'=> 'html',
                        'value' => function($dataProvider, $count){
                            $s = '';
                            if($dataProvider->id !== yii::$app->user->id){
                                $s.= '<li>';
                                $s.= Html::a( $dataProvider->username, $url = '/user/'.$dataProvider->id, $options = [] );
                                $s.='</li>';
                            }
                            return $s;
                        }
                    ]
                ],
            ]); ?>

        </div>
        <div class="col-lg-8">  <?php if(!empty(Yii::$app->request->get('user'))):
            $id = $_GET['user'];
            $user = $searchModel->findOne($id);
                if(!empty($user)):
            ?>
                <div class="row">
                    <div style="margin-top: 10%">
                        <div class="col-lg-4">
                            <?= Html::img($searchModel->setImage($id), [
                                'alt' => 'My logo',
                                'class' => 'img-circle',
                                'width' => "200",
                                'height' => "150"
                            ]) ?>
                        </div>
                        <div class="col-lg-5">
                            <l><?= $user->username ?></l><br>
                            <l><?= $user->email ?></l>
                        </div>
                        <div class="col-lg-3">
                            <button id="open-message" class="btn">message</button>
                        </div>
                    </div>

                </div>
                <div class="row divMessage">
                    <button id="x" class="btn">X</button>
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <?= $this->render('messages', compact(
                            'messages', 'recipientId','id'
                        )) ?>
                    </iew') ?>
                    <div class="input-message">
                        <div class="col-md-11 col-lg-11 col-sm-11">
                          <textarea id = 'text' type="text" wrap="soft"  ></textarea>
                            <input id = 'recipient_id' type="hidden" value="<?= $id ?>">
                            <input id = 'sending_id' type="hidden" value="<?= yii::$app->user->identity->id ?>">
                        </div>

                        <div class="col-md-1 col-lg-1 col-sm-1 fileInput" id="fileUpload" >
                            <div style="width: 25px; margin-top: 40%;"><span  class="glyphicon glyphicon-paperclip"></span></div>
                        </div>
                    </div>
                </div>
            </div>

                                <!--    data-toggle="modal" data-target="#exampleModalLong"-->
            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">File input</h5>
                            <button type="button" class="close fileinput-remove" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" value="<?= $id ?>">
                            <input type="file" id="fileToUpload" multiple="multiple" name="files[]" />
                            <?php /*FileInput::widget([
                                'attribute' => 'filesUpload',
                                'model' => new \common\models\Message(),
                                'options'=>[
                                    'multiple'=>true
                                ],
                                'pluginOptions' => [
                                    'showPreview' => true ,
                                    'showCaption' => false,
                                    'showRemove' => true,
                                    'showUpload' => true,
                                    'uploadUrl' => Url::to(['/user/file-upload']),
                                    'uploadExtraData' => [
                                        "Message[recipient_id]"=> $id,
                                        "Message[status]" => 0,
                                        "Message[sending_id]"=> yii::$app->user->identity->id,
                                        "Message[type]"=>"files",
                                    ],
                                    'maxFileCount' => 10
                                ]
                            ]);*/?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; endif; ?>
        </div>
    </div>