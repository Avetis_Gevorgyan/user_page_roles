<?php /* @var $recipientId[] common\models\Message */ ?>
<?php /* @var $messages common\models\Message */ ?>
<?php
    use yii\helpers\Html;
?>
<div id= "messages">
     <?php
        if(!empty($messages)){
            foreach($messages as $message){
                if(in_array(  $message->id, $recipientId )){
                     $message->status = 1;
                     $message->save();
    ?>
                    <div class="messigeDiv right">
                        <?php if($message->type == 'text'){ ?>
                    <p class="getMessage" title="<?= date("Y-M-d H:i:s", $message->created_at) ?>"><?= $message->message ?></p>
                        <?php } else {
                            $url = '/frontend/web/files/'.$message->sending_id."/".$message->recipient_id.'/'.$message->id.'.'.$message->type;
                            ?>
                            <p class="getMessage" title="<?= date("Y-M-d H:i:s", $message->created_at) ?>"><?= Html::img($url) ?></p>
                        <?php } ?>
                    </div>
            <?php }else{ ?>
        <div class="messigeDiv left" id = "id<?= $message->id ?>">
            <?php if($message->type == 'text'){ ?>
                <p class="sendMessage" title="<?= date("Y-M-d H:i:s", $message->created_at) ?>">
                    <button onClick='delet(<?= $message->id ?>)' class = "del">x</button>
                    <?= $message->message ?>
                </p>
            <?php } else {
                $url = '/frontend/web/files/'.$message->sending_id."/".$message->recipient_id.'/'.$message->id.'.'.$message->type;
                ?>
                <p class="sendMessage" title="<?= date("Y-M-d H:i:s", $message->created_at) ?>">
                    <button onClick='delet(<?= $message->id ?>)' class = "del">x</button>
                    <?php //Html::img($url, ['width'=>100, 'heigth' =>100]) ?></p>
                </p>
            <?php } ?>
        </div>
    <?php       }
            }
        }
    ?>
</div>
