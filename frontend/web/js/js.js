var scrolled = false;
$(document).ready(function(){
    $("#fileUpload").click(function() {
        $("#fileToUpload").trigger('click');

        $("#fileToUpload").on('change', function (e) {
            var files = new FormData($('#fileToUpload')[0]);
            var recipient_id = $("#recipient_id").val();
            var sending_id = $("#sending_id").val();
            $.ajax({
                url: '/user/send-messages',
                type: 'post',
                data: {
                    "Message[message]": '[files',
                    "Message[recipient_id]": recipient_id,
                    "Message[status]": 0,
                    "Message[sending_id]": sending_id,
                    "Message[type]": "files",
                },
                success: function (response) {
                    console.log(files);
                }
            });
        });
    });

    $("#messages").on('scroll', function(){
        scrolled=true;
    });
    setInterval("updateScroll()",1000);

    function Delet(id){
        $( ".messigeDiv").contextmenu(function() {
            console.log(id);
        });
    }
    $( "#open-message" ).click( function() {
        $( ".divMessage" ).addClass( "show" );
    });

    $( "#x" ).click(function() {
        $( ".divMessage" ).removeClass( "show" );
    });

    $("#text").keypress(function(e) {
        if (e.which == 13) {
            scrolled = false;
            var text = $.trim($("#text").val());
           text =  escapeHtml(text);
            if(text != ""){
            $("#text").val('');
            var recipient_id = $("#recipient_id").val();
            var sending_id = $("#sending_id").val();
            var div = document.createElement('div');
            div.setAttribute('class', 'messigeDiv');
            div.setAttribute('class', 'left');
            document.getElementById("messages").append(div);
            var texts = document.createElement('p');
            div.append(texts);
            texts.setAttribute('class', 'sendMessage');
                texts.innerHTML = text+"<button  class = 'del'>x</button>";
                $.ajax({
                    url: '/user/send-messages',
                    type: 'post',
                    data: {
                        "Message[message]": text,
                        "Message[recipient_id]": recipient_id,
                        "Message[status]": 0,
                        "Message[sending_id]": sending_id,
                        "Message[type]": "text",
                    },
                    success: function(response) {

                        var respons = JSON.parse(response);

                        div.setAttribute('id', 'id'+respons.id);
                        div.setAttribute('onclick', 'delet('+respons.id+')');

                        var send = document.createElement('span');
                        document.getElementById("messages").append(send);
                        send.innerHTML = '<p class="jsP">'+respons.message+'</p>';
                    }
                });
            }
            }

    });


});
function delet(id){
var xx = confirm("SDFsg");
    if(xx){
        $.ajax({
            url: '/user/delete',
            type: 'post',
            data: {
                id: id
            },
            success: function(response){
            $("#id"+id).remove();
            }
        });
    }
}

function strip(html)
{
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText;
}

function updateScroll(){
    if (document.getElementById("messages")) {
        if(!scrolled){
            var element = document.getElementById("messages");
            element.scrollTop = element.scrollHeight;
        }
    }

}

function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}